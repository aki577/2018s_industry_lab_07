package ictgradschool.industry.exceptions.arraysandexceptions;

public class IndexTooLowException extends Exception
    {
    public IndexTooLowException()
        {
        }

    public IndexTooLowException(String message)
        {
        super(message);
        }

    public IndexTooLowException(Throwable cause)
        {
        super(cause);
        }

    public IndexTooLowException(String message, Throwable cause)
        {
        super(message, cause);
        }
    }
