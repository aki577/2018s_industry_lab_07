package ictgradschool.industry.exceptions.ex06;

public class InvalidWordException extends Exception
    {
    public InvalidWordException()
        {
        }

    public InvalidWordException(String message)
        {
        super(message);
        }

    public InvalidWordException(String message, Throwable cause)
        {
        super(message, cause);
        }

    public InvalidWordException(Throwable cause)
        {
        super(cause);
        }
    }
