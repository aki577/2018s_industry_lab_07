package ictgradschool.industry.exceptions.ex06;

import ictgradschool.Keyboard;

public class SplitWord
    {
    public void start() throws ExceedMaxStringLengthException, InvalidWordException
        {
        System.out.print("Enter a string of at most 100 characters: ");
        String s = Keyboard.readInput();

        if (s.length() > 100) {
            throw new ExceedMaxStringLengthException("The string you put in was too long.");
        }
        split(s);
        }

    private void split(String s) throws InvalidWordException
        {
        String[] str = s.split(" ");
        for (int i = 0; i < str.length; i++) {
            if (str[i].length() > 0 && (str[i].charAt(0) >= '0') && (str[i].charAt(0) <= '9')) {
                throw new InvalidWordException("The word begins with number.");
            }

            if (str[i].length() > 0) {
                System.out.print(str[i].charAt(0) + " ");
            }
        }
        }

    public static void main(String[] args) throws ExceedMaxStringLengthException, InvalidWordException
        {
        new SplitWord().start();
        }
    }
