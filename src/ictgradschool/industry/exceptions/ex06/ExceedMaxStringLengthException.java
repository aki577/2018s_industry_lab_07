package ictgradschool.industry.exceptions.ex06;

public class ExceedMaxStringLengthException extends Exception
    {
    public ExceedMaxStringLengthException()
        {
        }

    public ExceedMaxStringLengthException(String message)
        {
        super(message);
        }

    public ExceedMaxStringLengthException(String message, Throwable cause)
        {
        super(message, cause);
        }

    public ExceedMaxStringLengthException(Throwable cause)
        {
        super(cause);
        }
    }
